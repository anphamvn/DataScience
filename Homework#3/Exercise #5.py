# Read the following financial function to more understand about numpy: https://docs.scipy.org/doc/numpy/reference/routines.financial.html.

# fv(rate, nper, pmt, pv[, when])	                Compute the future value.
# pv(rate, nper, pmt[, fv, when])	                Compute the present value.
# npv(rate, values)	                                Returns the NPV (Net Present Value) of a cash flow series.
# pmt(rate, nper, pv[, fv, when])	                Compute the payment against loan principal plus interest.
# ppmt(rate, per, nper, pv[, fv, when])	            Compute the payment against loan principal.
# ipmt(rate, per, nper, pv[, fv, when])	            Compute the interest portion of a payment.
# irr(values)	                                    Return the Internal Rate of Return (IRR).
# mirr(values, finance_rate, reinvest_rate)	        Modified internal rate of return.
# nper(rate, pmt, pv[, fv, when])	                Compute the number of periodic payments.
# rate(nper, pmt, pv, fv[, when, guess, tol, …])	Compute the rate of interest per period.

# # For each function, give a example and try it.
import numpy as np

#parameters
rate =0.12/12   #12% per year, 1% per month
nper =5*12      #5 years(60 months)
pmt =5000        #Paymen per month
pv =90000         #Present value
values =[-120000,29000,26000,30000,32000,36000] #he values of the time series of cash flows
finance_rate =0.1
reinvest_rate =0.12
print('Compute the future value')
print(np.fv(rate, nper, -pmt, -pv))
print('Compute the present value.')
print(np.pv(rate, nper, pmt, np.fv(rate, nper, pmt, -pv)))
print('Returns the NPV (Net Present Value) of a cash flow series.')
print(np.npv(rate, values)) #Compute by month (rate by month)
print('Compute the payment against loan principal plus interest.')
print(np.pmt(rate, nper, pv))
print('Compute the payment against loan principal.')
print(np.ppmt(rate, np.arange(1,12,1), nper, pv)) #first 12 months
print('Compute the interest portion of a payment.')
print(np.ipmt(rate, np.arange(1,12,1), nper, pv)) #first 12 months
print('Return the Internal Rate of Return (IRR).')
print(np.irr(values))
print('Modified internal rate of return.')
print(np.mirr(values, finance_rate, reinvest_rate))
print('Compute the number of periodic payments.')
print(np.nper(rate, -pmt, -pv))
print('Compute the rate of interest per period.')
print(np.rate(nper, -pmt, pv,0.0))