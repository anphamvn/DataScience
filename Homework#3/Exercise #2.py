# 2.Write a python script to get a number N from user then create 3D array N x N x N with random (float) values on interval [0,10].
# Find min, max and sum of array by 3 axis (i.e., deep, height and width).
import numpy as np
N=int(input('input a number'))
#print(N)
# create 3D array N x N x N with random (float) values on interval [0,10].
arr =np.random.uniform(low=1,high=10,size=(N,N,N))
print(arr)
# ○	 Find min, max and sum of array by 3 axis (i.e., deep, height and width).
print('Min in deep')
print(arr.min(0))
print('Max in deep')
print(arr.max(0))
print('Sum in deep')
print(arr.sum(0))
print('---------------------------------')
print('Min in height')
print(arr.min(1))
print('Max in height')
print(arr.max(1))
print('Sum in height')
print(arr.sum(1))
print('---------------------------------')
print('Min in width')
print(arr.min(2))
print('Max in width')
print(arr.max(2))
print('Sum in width')
print(arr.sum(2))
print('---------------------------------')


