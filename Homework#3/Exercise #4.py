# 4.	Write a python script to randomly generate a 2D array 10 x 8 of float values on interval [-10, 10].
# ○	And then, get a float number from user and find its closest values in the array. SOME GUIDES:
# i.	i) use argmin() function for your problem: https://docs.scipy.org/doc/numpy/reference/generated/numpy.argmin.html;
# ii.	Or reshape to 1D, subtract array to value and sort.
# ○	Optional: If you passed the above problem, extend it to find 3 closest values in the array.

import numpy as np
# generate a 2D array 10 x 8 of float values on interval [-10, 10]
arr=np.random.uniform(low=-10,high=10,size=(10,8))
print(arr.shape)
print(arr)

#get a float number from user
F =float(input('input a float number'))
print(F)
#subtract array with F
subtractarr=np.abs(arr-F)
#print(subtractarr)
# Method 1: Using Built-in Function
# use argmin() function to closest values in the array
ind = np.unravel_index(np.argmin(subtractarr, axis=None), subtractarr.shape)
#print first closest values
print('1st closest value by built-in function {} '.format(arr[ind]))
# Method 2:reshape to 1D, subtract array to value and sort
#reshape to 1D
def find_closet_values(arrname,num,position):
    #Reshape Array to 1D
    rearr =arrname.reshape(10*8)
    #subtract array with Num and convert to list
    larr =list(set(rearr))
    #sort list by subtract
    larr.sort(key =lambda x:np.abs(x-num))
    #get closest value by position
    closetnum =larr[position]
    return closetnum

#firstcloset
firstclosest=find_closet_values(arr,F,0)
print('1st closest value {}'.format(firstclosest))
#secondcloset
secondclosest=find_closet_values(arr,F,1)
print('2nd closest value {}'.format(secondclosest))
#thirdloset
thirdclosest=find_closet_values(arr,F,2)
print('3th closest value {}'.format(thirdclosest))
