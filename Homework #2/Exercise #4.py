#4.	Write a python script to randomly generate a list of N integers on interval [0,10] (N is a number given by user) and then:
#○	Create and print a histogram of the list (i.e., count frequency of occurrences).
#○	Manually calculate and print the statistics description of the list (i.e, max, min, mean, variance and standard deviation). You may need to import math library and use function sqrt for this calculation.
#○	Remove duplicates from the list
#------------------------------------------------------------
import numpy as np
import math
#function to count feq occurrences
def count_feq(lnumber):
    #define dictionaty
    lnumber.sort()
    dic_feq ={}
    for i in lnumber:
        if i in dic_feq: #exits in dic_feq
            dic_feq[i] +=1
        else: #not exist add new dic_feq
            dic_feq[i] =1
    return dic_feq
#max function
def calc_max(lnumber):
    nmax=0
    for i in lnumber:
        if i>nmax:
            nmax=i
    return nmax
#min function
def calc_min(lnumber):
    nmin=10
    for i in lnumber:
        if i<nmin:
            nmin=i
    return nmin
#mean function
def calc_mean(lnumber):
    nsum=0
    for i in lnumber:
        nsum+=i
    nmean =nsum/len(lnumber)
    return nmean
#variance function
def calc_variance(lnumber):
    nmean = calc_mean(lnumber)
    nsumdiff=0
    for i in lnumber:
       nsumdiff+=(i-nmean)**2
    return nsumdiff/len(lnumber)
#standard deviation function
def calc_std_deviation(lnumber):
    return math.sqrt(calc_variance(lnumber))

num =int(input('Enter length of list'))
l =list(np.random.randint(low=0, high=11, size=num))
print('List is: {}'.format(l))
#Create and print a histogram of the list (i.e., count frequency of occurrences).
print('Frequency of occurrences: {}'.format(count_feq(l)))
#Remove duplicates from the list
print('unique values: {}'.format(list(set(l))))
#Manually calculate and print the statistics description of the list

strprint = {'Max': calc_max(l), 'Min': calc_min(l), 'Mean': calc_mean(l), 'Variance': calc_variance(l),
            'Std deviation': calc_std_deviation(l)}

print('List statistics: {}'.format(strprint))
