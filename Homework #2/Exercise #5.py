#5.	Write a python script to generate a list of 100 numbers of float using the function y = x ^ 2 / (4*x), x on interval (0, 10] and linear space.
import numpy as np
#xlist= np.linspace(0,100,num=100, endpoint=True)[1:]
ylist = [x*x/(4*x) for x in np.linspace(0,1,num=100, endpoint=True)[1:]] #remove values at 0 position avoid division by zero
print(ylist)
