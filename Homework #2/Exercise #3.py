#3.	Given a string s = 'the output that tells you if the object is the type you think or not',
#○	sort the words of this string by alphabet.
#OUTPUT: 'if is not object or output tells that the the the think type you you'
#○	upper case all the first character of the words
#OUTPUT: 'The Output That Tells You If The Object Is The Type You Think Or Not'
#GUIDE: both use split and join of string
s = 'the output that tells you if the object is the type you think or not'
#print(s)
lstr =s.split(' ')
lstr.sort()
#output 1
print(' '.join(lstr))
#output 2
print(s.title())


