#6.	Write a python script to get sum of column of matrix N x M (i.e, list in a list). For example:
#M = [[1,2,3,4,5],
#        [3,4,2,5,6],
#        [1,6,3,2,5]]
#OUTPUT: [5, 12, 8, 11, 16]

#import numpy as np
#l =list(np.random.randint(low=1, high=100, size=(3,5)))
l=[[1,2,3,4,5],[3,4,2,5,6],[1,6,3,2,5]]
print(l)
lsum= [sum(row[i] for row in l) for i in range(len(l[0])) ]
#print(len(l[0]))
print(lsum)
