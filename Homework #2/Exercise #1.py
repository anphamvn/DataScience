#exercise #1
#1.	Write a python script that gets a string of number from user, e.g., 12, 32, 31, 12, 42, 21, 58, 92,37,45
#	Using this string to generate 2 list of numbers, the first list consists of even numbers and the second one is the list of odd numbers.
numstr =input('Enter a string of number')
#numstr
fulllist =[int(i) for i in numstr.split(',')]
#even list
evenlist =[i for i in fulllist if i%2==0]
#Odd list
oddlist=[i for i in fulllist if i%2!=0]
#print
print(evenlist)
print(oddlist)
