#2.Given the following list l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]], sort this list by sum of its item which has type is int or float (no recursive).
#OUTPUT:  [('21', 0.4, 4, [31, 3, 5]),  (1, 4, 'ewe', '5'),  {3, 5},  [7, 3, 's', 2],  [4, 2, 6, 'dad']]
#GUIDE: check type(x) is int or float
l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]
print(l)
l.sort(key= lambda x:sum(i for i in x if isinstance(i, int) ==True or isinstance(i, float) ==True ) )
print(l)
