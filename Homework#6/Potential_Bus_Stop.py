import pandas as pd
import requests
import json
import time
import folium
import os
import webbrowser
GOOGLEMAP_ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json'
#Read CSV file
bust_stops = pd.read_csv('Potentail_Bust_Stops.csv')
#print(data)
i=0
def get_intersection_location(row):
    # Get latitude and longitude from 2 streets intersection
    street1, street2 = row['Street_One'], row['Street_Two']
    print(time)
    params = {
        'address': street1 + "," + street2,
    }
    while True:
        try:
            response= requests.get(GOOGLEMAP_ENDPOINT, params= params)
            response = json.loads(response.text)
            time.sleep(2)
            data = response['results'][0]['geometry']['location']
            return data['lat'], data['lng']
        except Exception:
            pass

def splitlattlng(lattlong='',latt =True):
    strlattlong =str(lattlong)
    if latt:
        strreplace='('
        strreturn=strlattlong.split(',')[0]
    else:
        strreplace=')'
        strreturn=strlattlong.split(',')[1]
    return float(strreturn.replace(strreplace,''))

#pbs = data.apply(lambda row: get_intersection_location(row), axis=1)
bust_stops['latlong'] =bust_stops.apply(lambda row: get_intersection_location(row), axis=1)
bust_stops['latt'] =bust_stops.apply(lambda row: splitlattlng(row[2]), axis=1)
bust_stops['lng'] =bust_stops.apply(lambda row: splitlattlng(row[2],False), axis=1)

#drop column
bust_stops.drop('latlong', axis=1,inplace=True)

#world_map
latitude = 37.77
longitude = -122.42
# create map and display it
sanfran_map = folium.Map(location=[latitude, longitude], zoom_start=12)

bust_stops_map = folium.map.FeatureGroup()

# loop through the 100 crimes and add each to the incidents feature group
for lat, lng, pop in zip(bust_stops.latt, bust_stops.lng,bust_stops.Street_Two):
    bust_stops_map.add_child(
        folium.Marker(
            [lat, lng],
            popup=pop,
            icon=folium.Icon(color='green',icon='info-sign')
        )
    )

# add incidents to map
sanfran_map.add_child(bust_stops_map)

filepath = 'D:/AnPham/map.html'
sanfran_map.save(filepath)
webbrowser.open('file://' + filepath)