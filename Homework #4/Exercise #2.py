import pandas as pd
#Load the iris file to a DataFrame of Pandas
df=pd.read_csv('iris.data',sep=',')
df.columns= ['sepal length','sepal width','petal length','petal width','class']
print(df)
#Calculate the statistical description of sepal_length, sepal_width, petal_length and petal_width by type of flower
print(df.groupby('class').describe().T)
#For each type of flower, normalize its sepal_length and petal_length by replacing the outer line 50% distance to mean (i.e., x = (x + mean)/2 )
print( df[['class','sepal length','petal length']].groupby('class').transform(lambda x: (x - x.mean()) / x.std()))