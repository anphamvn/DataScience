import pandas as pd
import numpy as np
import random as rd
#Given a list of ids as follows: ids = [‘id1’,’id2’,’id3’,...,’id20’] (len(ids)=20).
l= list('id' + str(x) for x in range(1,21))
#S1 and S2 are randomly picked up values from ids (values in S1 and S2 are unique).
#using random lib
S1=pd.Series(rd.sample(l,10))
S2=pd.Series(rd.sample(l,10))
#S3 and s4 are randomly generated on interval [-100, 100]. Example of data output
S3= pd.Series(np.random.randint(low=-100,high=100,size=10))
S4=pd.Series(np.random.randint(low=-100,high=100,size=10))
#Build 2 DataFrame from those Series, df1 is a concatenation of S1, S3 and df2 is the concatenation of S2, S4
#using Dictionary
DF1 = pd.DataFrame({'key':S1,'data1':S3})
DF2 = pd.DataFrame({'key':S2,'data2':S4})
#print two DF
print(DF1)
print(DF2)
# Execute a join on those two dataframe by using merge() function
result = pd.merge(DF1,DF2,how='inner',on='key')
#prinr result
print(result)
