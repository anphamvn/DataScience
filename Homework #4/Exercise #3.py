import pandas as pd
import numpy as np
# Generate a DataFrame where the first column is a range of date in month (use date_range with freq = ‘M’ and periods = 50, starting from 2018-01-31
colMonth= pd.Series(pd.date_range('2018-01-31',periods=50,freq='M'))

# and the second one is a correspondent random number (called ‘sale’) in range(10,30)
colSales =pd.Series(np.random.randint(low=10,high=31,size=50))
#create dataframe
d={'Month':colMonth,'Sale':colSales}
df =pd.DataFrame(d)
#Create Year Column
#df['Year']= pd.to_datetime(df['Month']).dt.year
df=df.assign(Year=pd.Series(df['Month'].dt.year))
#Calculate the sum of sale by year
print(df.groupby('Year')['Sale'].sum())
#Calculate the sum of sale by Month
df=df.assign(MonthNum=pd.Series(df['Month'].dt.month))
print(df.groupby('MonthNum')['Sale'].sum())
