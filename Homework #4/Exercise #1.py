# ○	Given the following string
# “Ah meta descriptions… the last bastion of traditional marketing!
# The only cross-over point between marketing and search engine optimisation!
# The knife edge between beautiful branding and an online suicide note!”
# Split it into list of words and then use this list to create a Series of word.
# ○	Filter to extract all words that contains at least 3 vowels (i.e., ‘a’,’i’,’o’,’e’,’u’) of the Series.
import pandas as pd
#Split it into list of words and then use this list to create a Series of word.
listword="Ah meta descriptions… the last bastion of traditional marketing! " \
    "The only cross-over point between marketing and search engine optimisation! " \
    "The knife edge between beautiful branding and an online suicide note!".lower().split(' ')
#print(listword)
vowels='aioeu'
series =pd.Series(listword)
#print(series)
#Function count vowels
def count_vowels(strword):
    return sum(l in vowels for l in strword)
#use map and lambda function to build a mask
filerseries =series.map(lambda x:x if(count_vowels(x) >=3 ) else 0)
#print Result
print(filerseries[(filerseries!=0)] )



